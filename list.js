import React, { Component } from 'react'
import { View, Text, Button } from 'react-native'

class list extends Component {
  goToScreen1 = () => {
    this.props.history.push('/screen1', { mynumber: 20 })
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'black' }}>
        <Text style={{ color: 'white' }}>Screen2</Text>
        <Button title="GO GO Power Ranger" onPress={this.goToScreen1} />
      </View>
    )
  }
}
export default list