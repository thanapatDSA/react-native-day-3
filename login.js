
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Button, Alert, TextInput, TouchableOpacity } from 'react-native';

class login extends Component {
  state = {
    value: '',
    username: '',
    password: '',
    pic : 'https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-9/49517134_1993777407343087_2025443131536703488_o.jpg?_nc_cat=102&_nc_eui2=AeFKiTGgyV52QgjBZXttt1AoRA-epqLIS6j23hL4e9b-8ASYqQHZHwDFdORF4PbcQHHS991L5baSI1RNp8vre-bSuiCTEyIxPAyCzjA0q0d_Zg&_nc_ht=scontent-kut2-1.xx&oh=ed02b81d108e49a5a7ac8342f4b5e3a6&oe=5CF2A16D'
  }
  onValueChange = (field, value) => {
    this.setState({ [field]: value })
  }
  checkUsername = (user, password) => {
    if (user != 'admin' && password != 'eiei') {
      Alert.alert('Error', 'Username or Password wrong :(')
    } else {
      Alert.alert('Login Success', 'Yahoo!!!')
      this.goToProfile(user, this.state.pic)
    }
  }
  goToProfile = (user, pic) => {
    this.props.history.push('/profile', { user, pic })
  }
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image style={{ width: 250, height: 250, borderRadius: 250 / 2 }}
            source={{ uri: this.state.pic}} />
        </View>
        <View>
          <View style={styles.Button} />
          <Text style={styles.instructions} >{this.state.username} - {this.state.password}</Text>
          <TextInput placeholder='username'
            style={{ width: 300, height: 40, borderColor: 'gray', borderWidth: 1 }}
            onChangeText={value => { this.onValueChange("username", value) }} />
          <TextInput placeholder='password'
            secureTextEntry={true}
            style={{ width: 300, height: 40, borderColor: 'gray', borderWidth: 1 }}
            onChangeText={value => { this.onValueChange("password", value) }} />
          <Button title='Login'
            onPress={() => {
              this.checkUsername(this.state.username, this.state.password)
            }}
            style={{ width: 300, height: 40 }} />
        </View>
        <TouchableOpacity
          onPress={() => {
            Alert.alert('de', 'กรรม')
          }}>
          <Text style={styles.welcome}>ปิ้วๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆ</Text>
        </TouchableOpacity>
        </View>
        )
      }
    
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });

  export default login
