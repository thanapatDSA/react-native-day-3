import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Screen1 from './Screen1'
import Screen2 from './Screen2'
import Login from './login'
import Profile from './profile'
import component1 from './component1'
class Router extends Component {
  render() {
    return (
      <NativeRouter>
        <Switch>
          <Route exact path="/screen1" component={Screen1}/>
          <Route exact path="/screen2" component={Screen2}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/profile" component={Profile}/>
          <Route exact path="/component1" component={component1}/>
          <Redirect to='/screen1'/>
        </Switch>
      </NativeRouter>
    )
  }
}
export default Router