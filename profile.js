
import React, { Component } from 'react'
import { View, Text, StyleSheet,Button,Image } from 'react-native'

class Profile extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={{ color: 'black',fontSize: 25 }}>Profile</Text>
        <Image style={{ width: 250, height: 250, borderRadius: 250 / 2 }}
            source={{ uri: this.props.location.state.pic}} />
        <View>
          <Text style={styles.welcome}>Username</Text>
          <Text style={styles.instructions}>{this.props.location.state.user}</Text>
        </View>
        <View>
          <Text style={styles.welcome}>Frist name</Text>
          <Text style={styles.welcome}>Last name</Text>
        </View>
        <View style={{ width: '90%', height: 40 }}>
        <Button title='Edit'
            onPress={() => {
            }}
            style={{ width: '90%', height: 40 }} />
      </View>
      <View style={{ width: '90%', height: 40 }}>
        <Button title='Back'
            onPress={() => {
              this.props.history.push('./login')
            }}
            style={{ width: '90%', height: 40 }} />
      </View>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 15,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    fontSize: 20,
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default Profile