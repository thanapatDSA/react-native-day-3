import React, { Component } from 'react'
import { View, Text,Alert } from 'react-native'
import {Link} from 'react-router-native'
import { MyButton } from './component1'

class Screen1 extends Component {
 UNSAFE_componentWillMount(){
  console.log(this.props);
  if(this.props.location && this.props.location.state && this.props.location.state.mynumber)
  Alert.alert('your NUmber is',this.props.location.state.mynumber + '')
  }
  render() {
    return (
      <View style={{ flex: 1 ,backgroundColor:'red'}}>
        <MyButton><Text>Screen1</Text></MyButton>
        <Link to="/screen2">
          <Text style={{color: 'white'}}>Go Chage Screen2 !</Text>
        </Link>
      </View>
    )
  }
}
export default Screen1